﻿using Spaceship;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private float rateOfFire;
        private float rateOfTime;
        private bool firePerform;
        private bool held;
        private Vector2 movementInput = Vector2.zero;
        private ShipInputActions inputActions;
        private float minX;
        private float maxX;
        private float minY;
        private float maxY;

        private void Awake()
        {
            InitInput();
            CreateMovementBoundary();
            rateOfTime = rateOfFire;
        }
        
        private void Update()
        {
            Move();
            if (firePerform)
            {
                rateOfTime -= Time.deltaTime;
                if (rateOfTime <= 0)
                {
                    firePerform = false;
                    rateOfTime = rateOfFire;
                }
            }
            if (!held) return;
            if (firePerform) return;
            playerSpaceship.Fire();
            firePerform = true;
        }
        
        private void InitInput()
        {
            inputActions = new ShipInputActions();
            inputActions.Player.Move.performed += OnMove;
            inputActions.Player.Move.canceled += OnMove;
            inputActions.Player.Fire.performed += OnFire;
            inputActions.Player.ChangeBullet.performed += OnChange;
        }

        private void OnFire(InputAction.CallbackContext obj)
        {
            held = !held;
        }

        private void OnChange(InputAction.CallbackContext obj)
        {
            playerSpaceship.ChangeBullet();
        }

        private void OnMove(InputAction.CallbackContext obj)
        {
            if (obj.performed)
            {
                movementInput = obj.ReadValue<Vector2>();
            }
            
            if (obj.canceled)
            {
                movementInput = Vector2.zero; 
            }
        }

        private void Move()
        {
            var inputVelocity = movementInput * playerSpaceship.Speed;

            var position = transform.position;
            var newPosition = position;
            newPosition.x = position.x + inputVelocity.x * Time.smoothDeltaTime;
            newPosition.y = position.y + inputVelocity.y * Time.smoothDeltaTime;

            // Clamp movement within boundary
            newPosition.x = Mathf.Clamp(newPosition.x, minX, maxX);
            newPosition.y = Mathf.Clamp(newPosition.y, minY, maxY);

            position = newPosition;
            transform.position = position;
        }
        
        private void CreateMovementBoundary()
        {
            var mainCamera = Camera.main;
            Debug.Assert(mainCamera != null, "Main camera cannot be null");
            
            var spriteRenderer = playerSpaceship.GetComponent<SpriteRenderer>();
            Debug.Assert(spriteRenderer != null, "spriteRenderer cannot be null");
            
            var offset = spriteRenderer.bounds.size;
            minX = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).x + offset.x / 2;
            maxX = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).x - offset.x / 2;
            minY = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).y + offset.y / 2;
            maxY = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).y - offset.y * 4;
        }

        private void OnEnable()
        {
            inputActions.Enable();
        }

        private void OnDisable()
        {
            inputActions.Disable();
        }
    }
}
