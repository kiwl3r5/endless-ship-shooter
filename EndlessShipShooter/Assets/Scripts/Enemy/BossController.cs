﻿using Spaceship;
using UnityEngine;

namespace Enemy
{
    public class BossController : MonoBehaviour
    {
        [SerializeField] private Transform target;
        [SerializeField] private float speed;
        [SerializeField] private float rateOfFire;
        [SerializeField] private float rateOf;
        [SerializeField] private BossSpaceship bossSpaceship;

        private void Start()
        {
            target = GameObject.FindWithTag("Player").transform;
            speed = bossSpaceship.Speed;
        }

        private void Update()
        {
            if (target == null) return;
            MoveToPlayer(speed);
            Fire(rateOfFire);
        }

        private void MoveToPlayer(float speed)
        {
            var position = transform.position;
            var targetPosition = target.position;
            position = Vector2.MoveTowards (position, new Vector2(targetPosition.x, position.y), speed * Time.deltaTime);
            transform.position = position;
        }

        private void Fire(float rateOfFire)
        {
            rateOf -= Time.deltaTime;
            if (!(rateOf <= 0)) return;
            rateOf = rateOfFire;
            bossSpaceship.Fire();
        }
    }    
}