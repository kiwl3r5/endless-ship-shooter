﻿using System;
using Spaceship;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Button startButton;
        [SerializeField] private Button nextLevelButton;
        [SerializeField] private Button restartButton;
        [SerializeField] private TextMeshProUGUI highScoreText;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private BossSpaceship bossSpaceship;
        //[SerializeField] private EnemyController enemyController;
        public event Action OnRestarted;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;
        [SerializeField] private int bossSpaceshipHp;
        [SerializeField] private int bossSpaceshipMoveSpeed;
        private int scoreNum;
        private int sceneNum;
        //[SerializeField] private ScoreManager scoreManager;
        
        public static GameManager Instance => _instance;
        private static GameManager _instance;

        public void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(this.gameObject);
                return;
            }
            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
            
            
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            //Debug.Assert(scoreManager != null, "scoreManager cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(bossSpaceshipHp > 0, "bossSpaceshipHp > 0");
            Debug.Assert(bossSpaceshipMoveSpeed > 0, "bossSpaceshipMoveSpeed > 0");
            startButton.onClick.AddListener(OnStartButtonClicked);
            nextLevelButton.onClick.AddListener(NextScene);
            restartButton.onClick.AddListener(Restart);
        }

        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            StartGame();
        }

        public void NextScene()
        {
            sceneNum += 1;
            var res = sceneNum % 2;
            if (res == 0 && sceneNum != 10)
            {
                SceneManager.LoadScene("Game");
            }
            else
            {
                SceneManager.LoadScene("Game2");
            }
            //nextLevelButton.gameObject.SetActive(false);
            //gameObject.SetActive(true);
            dialog.gameObject.SetActive(false);
            if (sceneNum == 10)
            {
                Invoke(nameof(SpawnBossSpaceship),0.1f);
                SceneManager.LoadScene("BossRoom");
                sceneNum = 0;
            }
            else
            {
                Invoke(nameof(SpawnEnemySpaceship),0.1f);
            }
            //Invoke(nameof(SpawnPlayerSpaceship),0.1f);
        }
        private void StartGame()
        {
            //scoreManager.Init(this);
            //ScoreManager.Instance.Init(this);
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
            startButton.gameObject.SetActive(false);
        }
        
        //Player
        private void SpawnPlayerSpaceship()
        {
            var spaceship = Instantiate(playerSpaceship);
            spaceship.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spaceship.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            dialog.gameObject.SetActive(true);
            restartButton.gameObject.SetActive(true);
            nextLevelButton.gameObject.SetActive(false);
            highScoreText.text = $"High Score : {scoreNum}";
        }

        //Normal Enemy
        private void SpawnEnemySpaceship()
        {
            var spaceship = Instantiate(enemySpaceship);
            spaceship.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spaceship.OnExploded += OnEnemySpaceshipExploded;
        }
        private void OnEnemySpaceshipExploded()
        {
            scoreNum += 1;
            ScoreManager.Instance.SetScore(scoreNum);
            dialog.gameObject.SetActive(true);
            nextLevelButton.gameObject.SetActive(true);
            startButton.gameObject.SetActive(false);
            //scoreManager.SetScore(1);
        }

        //boss
        private void SpawnBossSpaceship()
        {
            var spaceship = Instantiate(bossSpaceship);
            spaceship.Init(bossSpaceshipHp, bossSpaceshipMoveSpeed);
            spaceship.OnExploded += OnEnemySpaceshipExploded;
        }
       
        public void Restart()
        {
            sceneNum = 0;
            scoreNum = 0;
            SceneManager.LoadScene("Game");
            restartButton.gameObject.SetActive(false);
            startButton.gameObject.SetActive(true);
            ScoreManager.Instance.SetScore(scoreNum);
            OnRestarted?.Invoke();
        }

        public void OnApplicationQuit()
        {
            Application.Quit();
            Debug.Log("Game Quit");
        }
    }
}
