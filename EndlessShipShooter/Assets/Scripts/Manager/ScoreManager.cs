﻿using TMPro;
using UnityEngine;

namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText;

        private GameManager gameManager;
        public static ScoreManager Instance => _instance;
        private static ScoreManager _instance;

        public void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(this.gameObject);
                return;
            }

            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
        }

        public void Init(GameManager gm)
        {
            gameManager = gm;
            gameManager.OnRestarted += OnRestarted;
            HideScore(false);
            SetScore(0);
        }

        public void SetScore(int score)
        {
            scoreText.text = $"Score : {score}";
        }
        
        private void OnRestarted()
        {
            GameManager.Instance.OnRestarted -= OnRestarted;
            //gameManager.OnRestarted -= OnRestarted;
            HideScore(true);
            SetScore(0);
        }

        private void HideScore(bool hide)
        {
            Instance.scoreText.gameObject.SetActive(!hide);
            //scoreText.gameObject.SetActive(!hide);
        }
    }
}


