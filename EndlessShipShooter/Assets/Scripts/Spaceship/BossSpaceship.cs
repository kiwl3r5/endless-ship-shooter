﻿using System;
using Manager;
using UnityEngine;
using UnityEngine.UI;

namespace Spaceship
{
    public class BossSpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;
        [SerializeField]private Image healthBar;
        [SerializeField]private Transform gunPos2;
        [SerializeField]private Transform gunPos3;
        
        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            SimplePool.Preload(defaultExplosion);
        }

        private void Start()
        {
            FullHp = Hp;
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }
        public void TakeHit(int damage)
        {
            Hp -= damage;
            healthBar.fillAmount = Hp / FullHp;
            if (Hp > 0)
            {
                return;
            }
            
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            AudioManager.Instance.Play("Explosion1");
            gameObject.SetActive(false);
            if (defaultExplosion != null)
            {
                SimplePool.Spawn(defaultExplosion, transform.position, Quaternion.identity);
            }
            OnExploded?.Invoke();
            Invoke(nameof(OnDestroy), 0.05f);
        }

        // ReSharper disable Unity.PerformanceAnalysis
        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, gunPosition.rotation);
            AudioManager.Instance.Play("Laser2");
            bullet.Init();
            Invoke(nameof(FireDelay),1f);
        }

        private void FireDelay()
        {
            var bullet2 = Instantiate(defaultBullet, gunPos2.position, gunPos2.rotation);
            var bullet3 = Instantiate(defaultBullet, gunPos3.position, gunPos3.rotation);
            bullet2.Init();
            bullet3.Init();
            AudioManager.Instance.Play("Laser2");
        }

        private void OnDestroy()
        {
            Destroy(gameObject);
        }
    }
}