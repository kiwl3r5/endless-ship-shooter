using System;
using Manager;
using UnityEngine;
using UnityEngine.UI;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;
        [SerializeField] private Image healthBar;
        [SerializeField] private Image bullet;
        [SerializeField] private Image noneSelectBullet;
        [SerializeField] private Sprite bullet1;
        [SerializeField] private Sprite bullet2;
        [SerializeField] Bullet specialBullet;
        private Bullet selectedBullet;
        bool isChange = false;
        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            Debug.Assert(specialBullet != null, "spacialBullet != null");
            selectedBullet = defaultBullet;
            SimplePool.Preload(defaultExplosion);
        }
        
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
            FullHp = Hp;
            DontDestroyOnLoad(gameObject);
        }

        public void ChangeBullet()
        {
            if (isChange == true)
            {
                selectedBullet = defaultBullet;
                bullet.sprite = bullet2;
                noneSelectBullet.sprite = bullet1;
                isChange = false;
            }
            else
            {
                selectedBullet = specialBullet;
                bullet.sprite = bullet1;
                noneSelectBullet.sprite = bullet2;
                isChange = true;
            }
        }

        public override void Fire()
        {
            var bullet = Instantiate(selectedBullet, gunPosition.position, Quaternion.identity);
            AudioManager.Instance.Play(isChange == false ? "Laser1" : "Laser3");
            bullet.Init();
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            healthBar.fillAmount = Hp / FullHp;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            AudioManager.Instance.Play("Explosion2");
            Destroy(gameObject);
            if (defaultExplosion != null)
            {
                SimplePool.Spawn(defaultExplosion, transform.position, Quaternion.identity);
            }
            OnExploded?.Invoke();
            Invoke(nameof(OnDestroy), 0.05f);
        }
        
        private void OnDestroy()
        {
            Destroy(gameObject);
        }
    }
}