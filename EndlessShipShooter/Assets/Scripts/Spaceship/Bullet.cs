using Manager;
using UnityEngine;

namespace Spaceship
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private int damage;
        [SerializeField] private float speed;
        [SerializeField] private new Rigidbody2D rigidbody2D;
        [SerializeField] private int bulletHp;
        public void Init()
        {
            Move();
        }

        private void Awake()
        {
            Debug.Assert(rigidbody2D != null, "rigidbody2D cannot be null");
        }

        private void Start()
        {
            Invoke(nameof(OnDestroy),3f);
        }

        private void Move()
        {
            rigidbody2D.velocity = transform.up * speed;
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            var target = other.gameObject.GetComponent<IDamagable>();
            target?.TakeHit(damage);
            if (target == null)
            {
                var bullet = other.gameObject.GetComponent<Bullet>();
                bullet.TakeHit(damage);
            }
            if (target != null)
            {
                AudioManager.Instance.Play("hit");
                OnDestroy();
            }
        }

        private void TakeHit(int dmg)
        {
            bulletHp -= dmg;
            if (bulletHp > 0)
            {
                return;
            }
            OnDestroy();
        }

        private void OnDestroy()
        {
            Destroy(gameObject);
        }
    }
}